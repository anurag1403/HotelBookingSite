<?php
//Template Name: Home Template 
get_header(); 
	/****** Home Slider ******/
	get_template_part('home','slider');
			  
	/****** Home colout ******/
	get_template_part('home','service');

	/****** Home gallery ******/
	get_template_part('home','gallery');
	 
	/****** Home blog ******/
	get_template_part('home','blog');

	/*==== Footer ====*/
get_footer(); 
 ?>
</div> <!-- /main-wrapper -->
