<?php get_header(); ?>
<div class="clearfix"></div>
<?php 
	$melbourne_options=hotel_melbourne_theme_default_data(); 
	$slider_setting = wp_parse_args(  get_option( 'melbourne_option', array() ), $melbourne_options ); ?>

<!----Blog slider------------------>
<?php if($slider_setting['post_slider_enabled'] == 1 ) { ?>
<div class="carousel-blog">
  <div class="col-md-6 col-sm-4 col-xs-12 nopadding">
    <div class="carousel-cell ">
	
      <div class="post-image"><?php if($slider_setting['home_slider_image_one']!=''){ ?> <img src="<?php echo esc_url($slider_setting['home_slider_image_one']); ?>" alt="<?php echo $slider_setting['home_slider_title_one']; ?>" class="img-responsive"><?php } ?> <span></span>  
		  <div class="srb-content blog-caption post-meta">
		     <div class="category"><small class="green"><?php if($slider_setting['home_slider_title_one']!=''){ echo $slider_setting['home_slider_title_one']; } ?> </small></div>
			 <a href="#">
              <h2><?php if($slider_setting['home_slider_desc_one']!=''){ echo $slider_setting['home_slider_desc_one']; } ?></h2>
             </a> 	
		  </div>
      </div>
    </div>
  </div>
  <div class="col-md-6 col-sm-4 col-xs-12 nopadding">
    <div class="carousel-cell ">
      <div class="post-image"> <?php if($slider_setting['home_slider_image_two']!=''){ ?><img src="<?php echo esc_url($slider_setting['home_slider_image_two']); ?>" alt="" class="img-responsive"><?php } ?> <span></span>
        <div class="srb-content blog-caption post-meta">
          <div class="category"><small class="red"><?php if($slider_setting['home_slider_title_two']!=''){ echo $slider_setting['home_slider_title_two']; } ?></small></div>
          
          <a href="#">
          <h2><?php if($slider_setting['home_slider_desc_two']!=''){ echo $slider_setting['home_slider_desc_two']; } ?></h2>
          </a> </div>
      </div>
    </div>
  </div>
  <div class="col-md-6 col-sm-4 col-xs-12 nopadding">
    <div class="carousel-cell ">
      <div class="post-image"><?php if($slider_setting['home_slider_image_three']!=''){ ?> <img src="<?php echo esc_url($slider_setting['home_slider_image_three']); ?>" alt="" class="img-responsive"><?php } ?> <span></span>
        <div class="srb-content blog-caption post-meta">
          <div class="category"><small class="purpal"><?php if($slider_setting['home_slider_title_three']!=''){ echo $slider_setting['home_slider_title_three']; } ?></small></div>
          
          <a href="#">
          <h2><?php if($slider_setting['home_slider_desc_three']!=''){ echo $slider_setting['home_slider_desc_three']; } ?></h2>
          </a> </div>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<?php hotel_melbourne_breadcrumbs(); ?>	
<script>
/*============================================
Blog carousel
==============================================*/

$('.carousel-blog').flickity({
  pageDots: false,
  wrapAround:true,
  imagesLoaded: true,
  freeScroll: true,
  autoPlay: 5000,
});
</script>
<!----/Blog slider------------------>

	<div class="clearfix"></div>	
<section class="blog-section">
	<div class="container ">
		<div class="row">
		<div class="col-md-8">
<?php
if(have_posts()):
while(have_posts()):
the_post();
 get_template_part( 'template-parts/content', get_post_format() );

endwhile;  ?>
<div class="blog-pagination pull-left" data-aos="fade-left">
				<?php echo paginate_links( array( 
							'show_all' => true,
							'prev_text' => '<', 
							'next_text' => '>',
							)); ?>
				</div>
				<?php  else : ?>
					<h2><?php _e( "Ooops!!... Post Not Found", 'hotel-melbourne' ); ?></h2>
			<div class="">
			<p><?php
			_e( "Sorry, but nothing matched your search criteria. Please try again with some different keywords.", 'hotel-melbourne' ); ?>
			</p>
			<?php get_search_form(); ?>
			</div>
				<div class="clearfix"></div>
				<?php endif; ?>	
</div>
<?php get_sidebar(); ?>
	</section>		
<div class="cearfix"></div>	
<?php get_footer(); ?>
</div>
